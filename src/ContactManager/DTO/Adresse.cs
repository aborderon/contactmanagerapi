using System.ComponentModel.DataAnnotations;

namespace Avico.DTO
{
    /// <summary>
    /// Contact DTO
    /// </summary>
    public class Adresse
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int NumeroRue { get; set; }
        [Required]
        public string NomRue { get; set; }
        [Required]
        public string NomVille { get; set; }
        [Required]
        public int CodePostal { get; set; }
        [Required]
        public int ContactId;
        public Contacts Contact;
    }
}