using System.Net.Sockets;
using System.Net;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Avico;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Xunit;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using System.Text;
using Avico.DTO;
using AspNetCore.Http.Extensions;

namespace ContactManager.FunctionnalTests
{
    public class ContactsControllerTests
    {
        private readonly TestServer _testServer;
       private readonly HttpClient _testClient;
       public ContactsControllerTests()
       {
           //Initializing the test environment
           _testServer = new TestServer(new WebHostBuilder()
               .UseStartup<Startup>());
           //Test client creation
           _testClient = _testServer.CreateClient();
       }


       // Getter on list empty
            [Fact]
            public async Task testGetAdresseEmptyAsync()
            {
                var response = await _testClient.GetAsync("/api/contacts/3/adresses");
                response.StatusCode.Should().Be(StatusCodes.Status404NotFound);
            }


            [Fact]
                public async Task testGetTelephoneEmptyAsync()
            {
                var response = await _testClient.GetAsync("/api/contacts/3/telephones");
                response.StatusCode.Should().Be(StatusCodes.Status404NotFound);
            }
       // End Getter with Delete


       // Getters
            [Fact]
            public async Task testGetContactsAsync()
            {
                var response = await _testClient.GetAsync("/api/contacts");
                response.StatusCode.Should().Be(StatusCodes.Status200OK);
                //response.EnsureSuccessStatusCode();
            }


            [Fact]
            public async Task testGetContactsByFakeRouteAsync()
            {
                var response = await _testClient.GetAsync("/api/contact");
                response.StatusCode.Should().Be(StatusCodes.Status404NotFound);
            }

            [Fact]
            public async Task testGetContactByIdAsync()
            {
                var response = await _testClient.GetAsync("/api/contacts/1");
                response.StatusCode.Should().Be(StatusCodes.Status200OK);
            }

            [Fact]
            public async Task testGetContactByFakeIdAsync()
            {
                var response = await _testClient.GetAsync("/api/contacts/-1");
                response.StatusCode.Should().Be(StatusCodes.Status404NotFound);
            }


            [Fact]
            public async Task testGetAdresseByContactIdAsync()
            {
                var response = await _testClient.GetAsync("/api/contacts/1/adresses");
                response.StatusCode.Should().Be(StatusCodes.Status200OK);
            }

            [Fact]
            public async Task testGetTelephoneByContactIdAsync()
            {
                var response = await _testClient.GetAsync("/api/contacts/1/telephones");
                response.StatusCode.Should().Be(StatusCodes.Status200OK);
            }


            [Fact]
            public async Task testGetTelephoneByContactFakeIdAsync()
            {
                var response = await _testClient.GetAsync("/api/contacts/-1/telephone");
                response.StatusCode.Should().Be(StatusCodes.Status404NotFound);
            }
       // End Getters




       // PUT
        [Fact]   
        public async Task testPutContactByIdAsync()
        {
            // Méthode Annexe en json
            var jsonString = "{\"id\":1,\"prenom\":\"Dylan\",\"nom\":\"Chauny\"}";
            var httpContent = new StringContent(jsonString, Encoding.UTF8, "application/json");
            
            var response = await _testClient.PutAsync("/api/contacts/1",httpContent);
            response.StatusCode.Should().Be(StatusCodes.Status200OK);
        }

        [Fact]   
        public async Task testPutContactForBadRequestAsync()
        {
            var contact = new Contacts {
                Nom = "Bad",
                Prenom = "Request"
            };

            var response = await _testClient.PutAsJsonAsync("api/contacts/1", contact);
            response.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
        }

        [Fact]
        public async Task testPutContactForNotFoundAsync()
        {
            var contact = new Contacts {
                Id = 200,
                Nom = "Not",
                Prenom = "Found"
            };

            var response = await _testClient.PutAsJsonAsync("api/contacts/200", contact);
            response.StatusCode.Should().Be(StatusCodes.Status404NotFound);
        }
    
       //Adresse 
            [Fact]   
            public async Task testPutAdresseByContactIdAsync()
            {
                var adresse1 = 1;
                var adresse  = new Adresse {
                        Id = adresse1,
                        NumeroRue = 20,
                        NomRue = "Rue des soges",
                        NomVille = "Paris",
                        CodePostal = 87100
                };

                var response = await _testClient.PutAsJsonAsync("api/contacts/1/adresses/1", adresse);
                response.StatusCode.Should().Be(StatusCodes.Status200OK);
            }

            [Fact]   
            public async Task testPutAdresseByContactIdForNotFoundContactAsync()
            {
                var adresse1 = 1;
                var adresse  = new Adresse {
                        Id = adresse1,
                        NumeroRue = 20,
                        NomRue = "Rue des soges",
                        NomVille = "Paris",
                        CodePostal = 87100
                };

                var response = await _testClient.PutAsJsonAsync("api/contacts/-2/adresses/1", adresse);
                response.StatusCode.Should().Be(StatusCodes.Status404NotFound);
            }

            [Fact]   
            public async Task testPutAdresseByContactIdForNotFoundAdresseAsync()
            {
                var adresseId = -1;
                var adresse  = new Adresse {
                        Id = adresseId,
                        NumeroRue = 20,
                        NomRue = "Rue des soges",
                        NomVille = "Paris",
                        CodePostal = 87100
                };

                var response = await _testClient.PutAsJsonAsync("api/contacts/1/adresses/"+adresseId, adresse);
                response.StatusCode.Should().Be(StatusCodes.Status404NotFound);
            }

            [Fact]   
            public async Task testPutAdresseByContactIdForBadRequestAdresseAsync()
            {
                var adresse  = new Adresse {
                        NumeroRue = 20,
                        NomRue = "Rue des soges",
                        NomVille = "Paris",
                        CodePostal = 87100
                };

                var response = await _testClient.PutAsJsonAsync("api/contacts/1/adresses/1", adresse);
                response.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
            }
       // End Adresse

       // Téléphone 
            [Fact]   
            public async Task testPutTelephoneByContactIdAsync()
            {
                var telephoneId = 1;
                var telephone  = new Telephone {
                        Id = telephoneId,
                        NumeroTelephone = 0619745676,
                };

                var response = await _testClient.PutAsJsonAsync("api/contacts/1/telephones/1", telephone);
                response.StatusCode.Should().Be(StatusCodes.Status200OK);
            }

            [Fact]   
            public async Task testPutTelephoneByContactIdForNotFoundContactAsync()
            {
                var telephoneId = 1;
                var telephone  = new Telephone {
                        Id = telephoneId,
                        NumeroTelephone = 0619745676,
                };

                var response = await _testClient.PutAsJsonAsync("api/contacts/-2/telephones/1", telephone);
                response.StatusCode.Should().Be(StatusCodes.Status404NotFound);
            }

            [Fact]   
            public async Task testPutTelephoneByContactIdForNotFoundTelephoneAsync()
            {
                var telephoneId = -1;
                var telephone  = new Telephone {
                        Id = telephoneId,
                        NumeroTelephone = 0619745676,
                };

                var response = await _testClient.PutAsJsonAsync("api/contacts/1/telephones/"+telephoneId, telephone);
                response.StatusCode.Should().Be(StatusCodes.Status404NotFound);
            }

            [Fact]   
            public async Task testPutAdresseByContactIdForBadRequestTelephoneAsync()
            {
                var telephone  = new Telephone {
                        NumeroTelephone = 0619745676,
                };

                var response = await _testClient.PutAsJsonAsync("api/contacts/1/telephones/1", telephone);
                response.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
            }
       // End Put
       

       // Delete   
       [Fact]   
       public async Task testDeleteByContactIdAsync()
       {
           var response = await _testClient.DeleteAsync("/api/contacts/1");
           response.StatusCode.Should().Be(StatusCodes.Status204NoContent);
       }

       [Fact]   
       public async Task testDeleteByFakeContactIdAsync()
       {
           var response = await _testClient.DeleteAsync("/api/contacts/-1");
           response.StatusCode.Should().Be(StatusCodes.Status404NotFound);
       }


       // Adresse 
            [Fact]   
            public async Task testDeleteAdressesIdByContactIdAsync()
            {
                var response = await _testClient.DeleteAsync("/api/contacts/1/adresses/1");
                response.StatusCode.Should().Be(StatusCodes.Status204NoContent);
            }

            [Fact]   
            public async Task testDeleteAdressesIdByFakeContactIdAsync()
            {
                var response = await _testClient.DeleteAsync("/api/contacts/-1/adresses/1");
                response.StatusCode.Should().Be(StatusCodes.Status404NotFound);
            }

            [Fact]   
            public async Task testDeleteFakeAdresseIdByContactIdAsync()
            {
                var response = await _testClient.DeleteAsync("/api/contacts/1/adresses/-1");
                response.StatusCode.Should().Be(StatusCodes.Status404NotFound);
            }
       // End Adresse 


       // Téléphone 
            [Fact]
            public async Task testDeleteTelephoneIdByContactIdAsync()
            {
                var response = await _testClient.DeleteAsync("/api/contacts/1/telephone/1");
                response.StatusCode.Should().Be(StatusCodes.Status204NoContent);
            }

            [Fact]   
            public async Task testDeleteTelephoneIdByFakeContactIdAsync()
            {
                var response = await _testClient.DeleteAsync("/api/contacts/-1/telephone/1");
                response.StatusCode.Should().Be(StatusCodes.Status404NotFound);
            }

            [Fact]   
            public async Task testDeleteFakeTelephoneIdByContactIdAsync()
            {
                var response = await _testClient.DeleteAsync("/api/contacts/1/telephone/-1");
                response.StatusCode.Should().Be(StatusCodes.Status404NotFound);
            }
       // End Delete
    }
}
