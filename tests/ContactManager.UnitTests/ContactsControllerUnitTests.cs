using System.Xml.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System;
using Xunit;
using FluentAssertions;
using Avico.Controllers;
using Avico.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace ContactManager.UnitTests
{
    public class ContactsControllerUnitTest
    {
        private ContactsController contactController = new ContactsController();


        // Getters
            [Fact]
            public void testGetAllContacts()
            {
                var result = contactController.GetContacts().Result;

                var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
                var contacts  = okResult.Value.Should().BeAssignableTo<IEnumerable<Contacts>>().Subject;

                contacts.Should().HaveCount(3);
            }


            [Fact]
            public void testGetContactsById()
            {
                int id = 3; 
        
                var result = contactController.GetContactById(id).Result;

                var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
                var contacts  = okResult.Value.Should().BeAssignableTo<Contacts>().Subject;

                contacts.Should().NotBeNull();          
                contacts.Nom.Should().Be("DUPOND");
                contacts.Prenom.Should().Be("Charles"); 
            }

            [Fact]
            public void testGetContactsByFakeId()
            {
                int id = -1; 
                var result = contactController.GetContactById(id).Result;

                var notFoundResult = result.Should().BeOfType<NotFoundResult>().Subject;
            }



            // Adresse        
                [Fact]   
                public void testGetAdresseByContactId()
                {
                    int id = 1; 
                    var result = contactController.GetAdressesByContactId(id).Result;

                    var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
                    var adresse  = okResult.Value.Should().BeAssignableTo<IEnumerable<Adresse>>().Subject;

                    adresse.Should().HaveCount(2);
                }

                [Fact]
                public void testGetAdressesEmptyByContactId()
                {
                    int id = 3; 
                    var result = contactController.GetAdressesByContactId(id).Result;

                    result.Should().BeOfType<NotFoundResult>();
                }
            // End Adresse


            // Telephone
                [Fact]   
                public void testGetTelephoneByContactId()
                {
                    int id = 1; 
                    var result = contactController.GetTelephonesByContactId(id).Result;

                    var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
                    var adresse  = okResult.Value.Should().BeAssignableTo<IEnumerable<Telephone>>().Subject;

                    adresse.Should().HaveCount(2);
                }


                [Fact]
                public void testGetTelephoneEmptyByContactId()
                {
                    int id = 3; 
                    var result = contactController.GetTelephonesByContactId(id).Result;

                    var notFoundResult = result.Should().BeOfType<NotFoundResult>();
                }
            // End Telephone
        // End Getters


        // Put
            //Contact
                [Fact]
                public void testPutContactByIdOK()
                {
                    int idContact = 3;
                    Contacts contactExpect = new Contacts {
                        Id = idContact,
                        Nom = "CHAUNY",
                        Prenom = "Dylan",
                    };

                    var actionResult = contactController.Put(idContact, contactExpect).Result;
                    var result = actionResult.Should().BeOfType<OkObjectResult>().Subject;
                    var contact = result.Value.Should().BeAssignableTo<Contacts>().Subject;

                    contact.Should().NotBeNull();
                    contact.Should().Be(contactExpect);
                }


                [Fact]   
                public void testPutContactForBadRequest()
                {
                    int idContact = 3;
                    var contactExpect = new Contacts {
                        Nom = "Bad",
                        Prenom = "Request"
                    };
                    var actionResult = contactController.Put(idContact, contactExpect).Result;
                    actionResult.Should().BeOfType<BadRequestResult>();
                }

                [Fact]
                public void TestPutContactForNotFound()
                {
                    int idContact = 200;
                    var contactExpect = new Contacts {
                        Id = idContact,
                        Nom = "Not",
                        Prenom = "Found"
                    };

                    var actionResult = contactController.Put(idContact, contactExpect).Result;
                    actionResult.Should().BeOfType<NotFoundResult>();
                }
            // End Contact

            // Adresse
                [Fact]   
                public void testPutAdresseByContactIdAsync()
                {
                    int idAdresse = 1;
                    int idContact = 1;
                    var adresseExpect  = new Adresse {
                            Id = idAdresse,
                            NumeroRue = 20,
                            NomRue = "Rue des soges",
                            NomVille = "Paris",
                            CodePostal = 87100
                    };

                    var actionResult = contactController.PutAdresse(idContact, idAdresse, adresseExpect).Result;
                    var result = actionResult.Should().BeOfType<OkObjectResult>().Subject;
                    var adresse = result.Value.Should().BeAssignableTo<Adresse>().Subject;

                    adresse.Should().NotBeNull();
                    adresse.Should().Be(adresseExpect);

                    adresse.NumeroRue.Should().Be(adresseExpect.NumeroRue);
                    adresse.NomRue.Should().Be(adresseExpect.NomRue);
                    adresse.NomVille.Should().Be(adresseExpect.NomVille);
                    adresse.CodePostal.Should().Be(adresseExpect.CodePostal);
                }

                [Fact]   
                public void testPutAdresseByContactIdForNotFoundContactAsync()
                {
                    int idAdresse = 1;
                    int idContact = -1;
                    var adresseExpect  = new Adresse {
                            Id = idAdresse,
                            NumeroRue = 20,
                            NomRue = "Rue des soges",
                            NomVille = "Paris",
                            CodePostal = 87100
                    };

                    var actionResult = contactController.PutAdresse(idContact, idAdresse, adresseExpect).Result;
                    actionResult.Should().BeOfType<NotFoundResult>();
                }

                [Fact]   
                public void testPutAdresseByContactIdForNotFoundAdresseAsync()
                {
                    int idAdresse = -1;
                    int idContact = 1;
                    var adresseExpect  = new Adresse {
                            Id = idAdresse,
                            NumeroRue = 20,
                            NomRue = "Rue des soges",
                            NomVille = "Paris",
                            CodePostal = 87100
                    };

                    var actionResult = contactController.PutAdresse(idContact, idAdresse, adresseExpect).Result;
                    actionResult.Should().BeOfType<NotFoundResult>();
                }

                [Fact]   
                public void testPutAdresseByContactIdForBadRequestAdresseAsync()
                {
                    int idAdresse = 1;
                    int idContact = 1;
                    var adresseExpect  = new Adresse {
                            NumeroRue = 20,
                            NomRue = "Rue des soges",
                            NomVille = "Paris",
                            CodePostal = 87100
                    };

                    var actionResult = contactController.PutAdresse(idContact, idAdresse, adresseExpect).Result;
                    actionResult.Should().BeOfType<BadRequestResult>();
                }
            // End Adresse


            // Telephone
                [Fact]   
                public void testPutTelephoneByContactIdAsync()
                {
                    int idTelephone = 1;
                    int idContact = 1;
                    var telephoneExpected  = new Telephone {
                            Id = idTelephone,
                            NumeroTelephone = 0664100822
                    };

                    var actionResult = contactController.PutTelephone(idContact, idTelephone, telephoneExpected).Result;
                    var result = actionResult.Should().BeOfType<OkObjectResult>().Subject;
                    var telephone = result.Value.Should().BeAssignableTo<Telephone>().Subject;

                    telephone.Should().NotBeNull();
                    telephone.Should().Be(telephoneExpected);

                    telephone.NumeroTelephone.Should().Be(telephoneExpected.NumeroTelephone);
                }

                [Fact]   
                public void testPutTelephoneByContactIdForNotFoundContactAsync()
                {
                    int idTelephone = 1;
                    int idContact = -1;
                    var telephoneExpected  = new Telephone {
                            Id = idTelephone,
                            NumeroTelephone = 0664100822
                    };

                    var actionResult = contactController.PutTelephone(idContact, idTelephone, telephoneExpected).Result;
                    actionResult.Should().BeOfType<NotFoundResult>();
                }

                [Fact]   
                public void testPutTelephoneByContactIdForNotFoundTelephoneAsync()
                {
                    int idTelephone = -1;
                    int idContact = 1;
                    var telephoneExpected  = new Telephone {
                            Id = idTelephone,
                            NumeroTelephone = 0664100822
                    };

                    var actionResult = contactController.PutTelephone(idContact, idTelephone, telephoneExpected).Result;
                    actionResult.Should().BeOfType<NotFoundResult>();
                }

                [Fact]   
                public void testPutTelephoneByContactIdForBadRequestTelephoneAsync()
                {
                    int idTelephone = 1;
                    int idContact = 1;
                    var telephoneExpected  = new Telephone {
                            NumeroTelephone = 0664100822
                    };

                    var actionResult = contactController.PutTelephone(idContact, idTelephone, telephoneExpected).Result;
                    actionResult.Should().BeOfType<BadRequestResult>();
                }
            // End Telephone
        // End Put


        // Delete
            [Fact]
            public void testDeleteByContactId()
            {
                int id = 3;
                var result = contactController.Delete(id);

                var okResult = result.Should().BeOfType<NoContentResult>().Subject;
            }

            [Fact]   
            public void TestDeleteByFakeContactId()
            {
                int id = -1;
                var result = contactController.Delete(id);

                var okResult = result.Should().BeOfType<NotFoundResult>().Subject;
            }

            // Adresse
                [Fact]   
                public void testDeleteAdressesIdByContactId()
                {
                    int idContact = 1;
                    int idAdresse = 1;
                    var result = contactController.DeleteAdresse(idContact,idAdresse);

                    var okResult = result.Should().BeOfType<NoContentResult>().Subject;
                }

                [Fact]   
                public void testDeleteAdressesIdByFakeContactId()
                {
                    int idContact = -1;
                    int idAdresse = 1;
                    var result = contactController.DeleteAdresse(idContact,idAdresse);

                    var okResult = result.Should().BeOfType<NotFoundResult>().Subject;
                }

                [Fact]   
                public void testDeleteFakeAdresseIdByContactId()
                {
                    int idContact = 1;
                    int idAdresse = -1;
                    var result = contactController.DeleteAdresse(idContact,idAdresse);

                    var okResult = result.Should().BeOfType<NotFoundResult>().Subject;
                }
            // End Adresse


            // Téléphone
                [Fact]   
                public void testDeleteTelephoneIdByContactId()
                {
                    int idContact = 1;
                    int idTelephone = 1;
                    var result = contactController.DeleteTelephone(idContact,idTelephone);

                    var okResult = result.Should().BeOfType<NoContentResult>().Subject;
                }

                [Fact]   
                public void testDeleteTelephoneIdByFakeContactId()
                {
                    int idContact = -1;
                    int idTelephone = 1;
                    var result = contactController.DeleteTelephone(idContact,idTelephone);

                    var okResult = result.Should().BeOfType<NotFoundResult>().Subject;
                }

                [Fact]   
                public void testDeleteFakeTelephoneIdByContactId()
                {
                    int idContact = 1;
                    int idTelephone = -1;
                    var result = contactController.DeleteTelephone(idContact,idTelephone);

                    var okResult = result.Should().BeOfType<NotFoundResult>().Subject;
                }
            // End Téléphone
        // End Delete
    }
}
